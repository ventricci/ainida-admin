//nome
//telefone
//email
//data

interface SET_NAME {
    type: "SET_NAME";
    name: string;
  }
  
  interface CLEAR_NAME {
    type: "CLEAR_NAME";
  }
  
  export function setName(name: string): SET_NAME {
    return {
      type: "SET_NAME",
      name
    };
  }
  
  export function clearName(): CLEAR_NAME {
    return {
      type: "CLEAR_NAME"
    };
  }
  
  type Action = SET_NAME | CLEAR_NAME;
  
  //State
  interface PedidosState {
    name: string;
    phoneNumber: string;
    email: string;
    data:Date;
  }
    //nome
    //telefone
    //email
    //data

  let INITIAL_STATE: PedidosState = {
    name: " ",
    phoneNumber: " ",
    email: " ",
    data: new Date()

  };
  
  //Reducer
  export default function userReducer(
    state = INITIAL_STATE,
    action: Action
  ): PedidosState {
    //console.log(action);
    switch (action.type) {
      case "SET_NAME":
        return { ...state, name: action.name };
  
      case "CLEAR_NAME":
        return { ...state, name: "" };
  
      default:
        return state;
    }
  }
  