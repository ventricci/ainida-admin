import React from "react";
import '../App.css';

export default function InputPadrao(props: { placeHolder: string}) {
  return (
    <div className="form-group">
        <input className="input-field" type="text" placeholder={props.placeHolder}/>
    </div>
  );
}