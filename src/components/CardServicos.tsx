import React from "react";
import "../Styles/ButtonStyles.css";
import "../Styles/CardStyle.css";
import { useHistory } from "react-router";

export default function CardServicos(props: { currentUser: { id: number; name: string; address: string ; profilePic: string} }) {
  
    let history = useHistory();

    function handleClick() {
      history.push("/servicos/" + props.currentUser.id.toString());
    }
    console.log(props.currentUser)
  return (
    <div className="cardServico">
      <div id="pedir">
        <div className="button">
          <div
            className="buttonBlack"
            onClick={() => {
              handleClick();
            }}
          >
            <text>pedir serviço</text>
          </div>
        </div>
      </div>
      <div id="nome"> {props.currentUser.name}</div>
      <div id="endereco"> {props.currentUser.address}</div>
      <div id="imagem">
        <img src={props.currentUser.profilePic}/>
      </div>
    </div>
  );
}
