import React from "react";
import '../Styles/ButtonStyles.css';

//import { useDispatch } from "react-redux";
//import { setName } from "../reducers/User";

export default function Button(props: { textToShow: string; buttonType: string; buttonHandleClick :()=>any;})
{

  return (
    <div className="button">
      <div
        className= {props.buttonType}
        
        onClick={() => {
          props.buttonHandleClick();
        }}
      >
        <text>{props.textToShow}</text>
      </div>
    </div>
  );
}
