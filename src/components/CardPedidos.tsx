import React from "react";
import "../Styles/ButtonStyles.css";
import "../Styles/CardStyle.css";
import Button from "./Button";

export function CardPedidosPendentes(props: { name: string; address: string }) {
  // let dispatch = useDispatch();

  return (
    <div className="cardPedido">
      <div id="nome"> {props.name}</div>
      <div id="contato"> {props.address}</div>
      <div id="ligar">
        <Button
          textToShow={"Ligar"}
          buttonType={"buttonWhite"}
          buttonHandleClick={() => console.log("funcionou")}
        />
      </div>
      <div id="email">
        <Button
          textToShow={"Mandar Email"}
          buttonType={"buttonWhite"}
          buttonHandleClick={() => console.log("funcionou")}
        />
      </div>
      <div id="excarq">
        <Button
          textToShow={"Arquivar"}
          buttonType={"buttonBlack"}
          buttonHandleClick={() => console.log("funcionou")}
        />
      </div>
    </div>
  );
}
export function CardPedidosArquivados(props: {
  name: string;
  address: string;
}) {
  // let dispatch = useDispatch();

  return (
    <div className="cardPedido">
      <div id="nome"> {props.name}</div>
      <div id="contato"> {props.address}</div>
      <div id="ligar">
        <Button
          textToShow={"Ligar"}
          buttonType={"buttonWhite"}
          buttonHandleClick={() => console.log("funcionou")}
        />
      </div>
      <div id="email">
        <Button
          textToShow={"Mandar Email"}
          buttonType={"buttonWhite"}
          buttonHandleClick={() => console.log("funcionou")}
        />
      </div>
      <div id="excarq">
        <Button
          textToShow={"Excluir"}
          buttonType={"buttonRed"}
          buttonHandleClick={() => console.log("funcionou")}
        />
      </div>
    </div>
  );
}
