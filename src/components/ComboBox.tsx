import React from "react";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../App.css";

//Componente
function ComboBox() {
  const [categoria, setCategoria] = React.useState("");
  const handleChange = (event: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setCategoria(event.target.value);
  };

  return (
    <div className="form-group">
      <select className="select-icon" value={categoria} onChange={handleChange}>
        <option value="todos">Todos</option>
        <option value="pedreiros">Pedreiros</option>
        <option value="encanador">Encanadores</option>
      </select>
      <FontAwesomeIcon className="icon" icon={faArrowDown} style={{gridArea:"icon"}} />
    </div>
  );
}

export default ComboBox;
