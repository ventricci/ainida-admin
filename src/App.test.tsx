import React, { useState } from "react";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./App.css";

//Componente
function App() {
  return (
    <div className="container">
      <div className="form-group">
        <div className="picture-wrapper">
          <FontAwesomeIcon className="iconFoto" icon={faPlus} />
        </div>
        <i>foto</i>
      </div>
    </div>
  );
}

export default App;
